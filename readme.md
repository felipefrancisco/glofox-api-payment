# Glofox's Software Developer Technical Assessment
##### Felipe Francisco

---

## Summary

This project was created using **PHP 7.0.13**, **Laravel 5.4** and **MongoDB**. Laravel was used just for handling the 
dependency injection, routes and to provide a single-command server setup.  However, the concepts usedin this project are framework-agnostic.
Every created classes used to construct the API
can be found in the `./app` folder.

- In this document, the term `<entity>` refers to both `Payments` and `Charges`.

- Each entity has its own folder (`./app/Library/Payments` and `./app/Library/Charges`) where you can find `Transformers`, 
  `Validators`, `Factories` and custom `Exceptions` used to create the API.

- For handling the charge's amount transformation, which depends on its payment type, I created `Chargers`, which will
  charge the amount accordingly by using the `->charge($amount)` method. These can be found on `./app/Payments/Chargers`

- Validations are being performed both in the `Request Layer` and in the `Service Layer`. 
    1. This approach uses the concepts of `Custom Requests`, which can be found in `./app/<entity>/Requests`.
    2. Each one of these requests implements `Validators`, which can be found in `./app/<entity>/Validators`.
        1. **"cc"** payments and **"dd"** payments have different validators, which can be found in `./app/Payments/Validators`.

- Data presentation is being handled by Fractal `Transformers`, which can be found in `./app/<entity/Transformers`.

- Factories were registered as `Singletons` in the main `AppServiceProvider`, which can be found in `./app/Providers`.

- Every created classes are PSR-1 & PSR-2 compliant. You can check this by running the following commands 
after installing dependencies:
    1. PSR-1 `./vendor/bin/phpcs --standard=PSR1 ./app`
    1. PSR-2 `./vendor/bin/phpcs --standard=PSR2 ./app`

## Validation

- The task description didn't include validation for some fields, so I added the following:
    1. `ccv` needs to be numeric, 3 or 4 digits. (ex. 123, 1234)
    2. `expiry` needs to be in the `YY/MMM` format.
    
Validation details can be found in `./app/<entity>/Validators`.

---

## Installation
```
# Clone Project
git clone https://bitbucket.org/felipefrancisco/glofox-api-payment.git glofox-api-payment
cd glofox-api-payment

# Install Dependencies
composer install
```

## Database Setup

We need to setup MongoDB before using the application.

```
# Start Mongod Service
service mongod start

# Connect to Mongo
mongo
use glofox

# Create Database User
db.createUser(
{
    user: "test",
    pwd: "aGlyZSBtZSE=",
    roles: [ "root" ]
})
```

Create database & collections by executing the `migration` command:
```
php artisan migrate
```
Migrations can be found in `./database/migrations`

## Environment Variables

If you want to change the database user (among other settings), the enviroment variables can be found in `./.env`.

## Server Configuration

Run the server:
```
php artisan serve
```

This is a Laravel based application, so executing the following command will launch an instance of the application 
on  `http://127.0.0.1:8000`

---

## Using the API

Since this is a backend-only API, there is no front-end. I've included a [Postman](https://www.getpostman.com/) collection file 
in `./resources/postman/`, so you can easily test this api with the predefined examples.

## Sample Execution

I've created a sample script that will execute all the API operations via cli. The `sandbox.php` file is located in the root 
folder of the project, you just need to execute the following command:
```
# Launch server
php artisans serve

# Open another terminal instance and execute:
php sandbox.php
```

The output should be like `./resources/sandbox/output.txt`

---
Do not hesitate to contact me :)


Felipe Francisco - felipefrancisco@outlook.com - +353 (0) 83 851 8599