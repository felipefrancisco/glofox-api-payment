<?php

require __DIR__.'/bootstrap/autoload.php';

$app = require_once __DIR__.'/bootstrap/app.php';

$sandbox = new \App\Library\Sandbox();
$sandbox->run();
