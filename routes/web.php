<?php

Route::get('/', function () {
    return view('glofox.home');
});


$options = [
    'prefix' => 'api',
    //'middleware' => 'auth:api'
];

Route::group($options, function () {

    /**
     * @api {post} /api/payment Create a Payment
     * @apiName CreatePayment
     * @apiGroup Payment
     */
    Route::post('/payment', 'PaymentController@create');

    /**
     * @api {post} /api/charge Create a Charge
     * @apiName CreateCharge
     * @apiGroup Charge
     */
    Route::post('/charge', 'ChargeController@create');

    /**
     * @api {get} /api/charge List all Charges
     * @apiName ListCharges
     * @apiGroup Charge
     */
    Route::get('/charge', 'ChargeController@list');

    /**
     * @api {get} /api/charge/{id} Find a Chager by its identifier.
     * @apiName ViewCharge
     * @apiGroup Charge
     *
     * @apiParam {String} id Charge Identifier.
     */
    Route::get('/charge/{id}', 'ChargeController@view');
});
