<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Payment API - Glofox Assessment</title>

        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <style>
            body {
                font-family: 'Raleway';
                text-align: center;
            }
        </style>
    </head>
    <body>
        @yield('content')
    </body>
</html>
