<?php

use Illuminate\Support\Facades\Schema;
use Jenssegers\Mongodb\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Initial extends Migration
{
    /**
     * The name of the database connection to use.
     *
     * @var string
     */
    protected $connection = 'mongodb';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasCollection('payments')) {
            Schema::create('payments', function (Blueprint $collection) {
                $collection->string('name');
                $collection->string('type');
                $collection->string('iban');
                $collection->date('expiry');
                $collection->string('cc');
                $collection->string('ccv');
            });
        }

        if (!Schema::hasCollection('charges')) {
            Schema::create('charges', function (Blueprint $collection) {
                $collection->string('payment_id');
                $collection->float('amount');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payments');
        Schema::drop('charges');
    }
}
