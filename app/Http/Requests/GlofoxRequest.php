<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

/**
 * Class GlofoxRequest
 * @package App\Http\Requests
 */
class GlofoxRequest extends FormRequest
{
    protected $defaultErrorCode = 400;
    /** @noinspection PhpMissingParentCallCommonInspection */

    /**
     * @param array $errors
     * @return JsonResponse
     */
    public function response(array $errors)
    {
        foreach ($errors as &$error) {
            $error = implode(',', $error);
        }

        $message = implode(" ", $errors);

        $response = [
            'message' => $message,
            'code' => $this->defaultErrorCode
        ];

        return new JsonResponse($response, $this->defaultErrorCode);
    }
}
