<?php
/**
 * Created by PhpStorm.
 * User: Felipe Francisco
 * Date: 09/25/2016
 * Time: 12:44 AM
 */

namespace App\Http\Controllers;

use App\Library\Payments\Requests\CreatePayment;
use App\Services\PaymentService;
use App\Transformers\PaymentTransformer;
use Illuminate\Http\Request;

/**
 * Class PaymentController
 * @package App\Http\Controllers
 */
class PaymentController extends Controller
{
    /**
     * PaymentService instance.
     * @var PaymentService
     */
    protected $paymentService;

    /**
     * Request instance.
     * @var Request
     */
    protected $request;

    /**
     * PaymentController constructor.
     * @param PaymentService $paymentService
     */
    public function __construct(PaymentService $paymentService)
    {
        $this->paymentService = $paymentService;
    }

    /**
     * @param CreatePayment $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(CreatePayment $request)
    {
        try {
            $data = $request->json()->all();

            $payment = $this->paymentService->create($data);

            $response = (new PaymentTransformer())->transform($payment);

            return response()->json($response)->setStatusCode(200);
        } catch (\Exception $e) {
            $code = 400;

            $message = [
                'message' => $e->getMessage(),
                'code' => $code
            ];

            return response()->json($message)->setStatusCode($code);
        }
    }
}
