<?php
/**
 * Created by PhpStorm.
 * User: Felipe Francisco
 * Date: 09/25/2016
 * Time: 12:44 AM
 */

namespace App\Http\Controllers;

use App\Library\Charges\Repositories\ChargeRepository;
use App\Library\Charges\Requests\CreateCharge;

use App\Services\ChargeService;
use App\Transformers\ChargeTransformer;
use App\Transformers\ListChargeTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

/**
 * Class ChargeController
 * @package App\Http\Controllers
 */
class ChargeController extends Controller
{
    /**
     * ChargeService instance.
     * @var ChargeService
     */
    protected $chargeService;

    /**
     * ChargeRepository instance.
     * @var ChargeRepository
     */
    protected $chargeRepository;

    /**
     * ChargeTransformer instance.
     * @var ChargeTransformer
     */
    protected $chargeTransformer;

    /**
     * ListChargeTransformer instance.
     * @var ListChargeTransformer
     */
    protected $listChargeTransformer;

    /**
     * Request instance.
     * @var Request
     */
    protected $request;

    /**
     * ChargeController constructor.
     * @param ChargeService $chargeService
     * @param ChargeRepository $chargeRepository
     * @param ChargeTransformer $chargeTransformer
     * @param ListChargeTransformer $listChargeTransformer
     */
    public function __construct(
        ChargeService $chargeService,
        ChargeRepository $chargeRepository,
        ChargeTransformer $chargeTransformer,
        ListChargeTransformer $listChargeTransformer
    ) {
        $this->chargeService = $chargeService;
        $this->chargeRepository = $chargeRepository;
        $this->chargeTransformer = $chargeTransformer;
        $this->listChargeTransformer = $listChargeTransformer;
    }

    /**
     * @param CreateCharge $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(CreateCharge $request)
    {
        try {
            $data = $request->json()->all();

            $payment = $this->chargeService->create($data);

            $response = $this->chargeTransformer->transform($payment);

            return response()->json($response)->setStatusCode(200);
        } catch (\Exception $e) {
            $code = 400;

            $message = [
                'message' => $e->getMessage(),
                'code' => $code
            ];

            return response()->json($message)->setStatusCode($code);
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function list()
    {
        try {
            /** @var Collection $charges */
            $charges = $this->chargeRepository->all();

            foreach ($charges as $key => $charge) {
                $charges->offsetSet($key, $this->listChargeTransformer->transform($charge));
            }

            $response = $charges->toArray();

            return response()->json($response)->setStatusCode(200);
        } catch (\Exception $e) {
            $code = 400;

            $message = [
                'message' => $e->getMessage(),
                'code' => $code
            ];

            return response()->json($message)->setStatusCode($code);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function view($id)
    {
        try {
            $charge = $this->chargeRepository->find($id);

            $response = $this->chargeTransformer->transform($charge);

            return response()->json($response)->setStatusCode(200);
        } catch (ModelNotFoundException $e) {
            $code = 404;

            $message = [
                'error' => "Charge doesn't exist"
            ];

            return response()->json($message)->setStatusCode($code);
        } catch (\Exception $e) {
            $code = 400;

            $message = [
                'message' => $e->getMessage(),
                'code' => $code
            ];

            return response()->json($message)->setStatusCode($code);
        }
    }
}
