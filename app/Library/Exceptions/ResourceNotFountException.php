<?php

namespace App\Library\Exceptions;

/**
 * Class ResourceNotFountException
 * @package App\Library\Exceptions
 */
class ResourceNotFountException extends \ErrorException
{
    protected $message = "Resource Not Found.";
}
