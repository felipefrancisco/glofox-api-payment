<?php
/**
 * Created by PhpStorm.
 * User: felipefrancisco
 * Date: 05/08/17
 * Time: 15:57
 */

namespace App\Library\Validators;

use Illuminate\Contracts\Validation\Factory as ValidationFactory;
use Illuminate\Support\Collection;
use Illuminate\Validation\ValidationException;
use Illuminate\Validation\Validator as IlluminateValidator;

/**
 * Class Validator
 * @package App\Library\Validators
 */
abstract class Validator
{
    /**
     * @var ValidationFactory
     */
    protected $validationFactory;

    /**
     * @var IlluminateValidator
     */
    protected $validator;

    /**
     * @return Collection
     */
    abstract public function rules() : Collection;

    /**
     * Validator constructor.
     * @param ValidationFactory $validationFactory
     */
    public function __construct(ValidationFactory $validationFactory)
    {
        $this->validationFactory = $validationFactory;
    }

    public function validate()
    {
        if (! $this->validator->passes()) {
            throw new ValidationException($this->validator);
        }
    }

    /**
     * @param $data
     * @return $this
     */
    public function prepare($data)
    {
        $this->validator = $this->validationFactory->make(
            $data,
            $this->rules()->toArray()
        );

        return $this;
    }
}
