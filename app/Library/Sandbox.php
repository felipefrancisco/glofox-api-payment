<?php

namespace App\Library;

use GuzzleHttp\Exception\ConnectException;
use Symfony\Component\Console\Output\ConsoleOutput;

/**
 * Class Sandbox
 * @package App\Library
 */
class Sandbox
{
    /**
     * Sandbox constructor.
     */
    public function __construct()
    {
        $this->output = new ConsoleOutput();
        $this->client = new \GuzzleHttp\Client();
    }

    public function run()
    {
        try {
            $payment = $this->createCreditCardPayment();
            $charge = $this->createCharge($payment->_id);
            $this->viewCharge($charge->_id);

            $payment = $this->createDirectDebitPayment();
            $charge = $this->createCharge($payment->_id);
            $this->viewCharge($charge->_id);

            $this->listCharges();
        } catch (ConnectException $e) {
            $this->output->writeln("<comment>Couldn't connect to 127.0.0.1:8000, please execute `php artisan serve` before running this file.</comment>");
        }
    }

    /**
     * @param $method
     * @param $url
     * @param array $options
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    private function request($method, $url, $options = [])
    {
        $message = "\n<question>". $method ." ". $url ."</question>";
        $this->output->writeln($message);

        return $this->client->request($method, $url, $options);
    }

    /**
     * @return mixed
     */
    private function createCreditCardPayment()
    {
        $response = $this->request('POST', 'http://127.0.0.1:8000/api/payment', [
            'json' => [
                "name" => "My Credit Card",
                "type" => "cc",
                "cc" => "4444 4444 4444 4444",
                "ccv" => "123",
                "expiry" => "02/2010"
            ]
        ]);

        $contents = $response->getBody()->getContents();

        $decoded = json_decode($contents);

        $message = "Payment (CC) Created:";
        $this->output->writeln($message);

        $message = '<comment>'. $contents .'</comment>';
        $this->output->writeln($message);

        return $decoded;
    }

    /**
     * @return mixed
     */
    private function createDirectDebitPayment()
    {
        $response = $this->request('POST', 'http://127.0.0.1:8000/api/payment', [
            'json' => [
                "name" => "My Direct Debit Account",
                "type" => "dd",
                "iban" => "IE95BOFI90909090909090"
            ]
        ]);

        $contents = $response->getBody()->getContents();
        $decoded = json_decode($contents);

        $message = "Payment (DD) Created:";
        $this->output->writeln($message);

        $message = '<comment>'. $contents .'</comment>';
        $this->output->writeln($message);

        return $decoded;
    }

    /**
     * @param $paymentId
     * @return mixed
     */
    private function createCharge($paymentId)
    {
        $response = $this->request('POST', 'http://127.0.0.1:8000/api/charge', [
            'json' => [
                "amount" => 100,
                "payment_id" => $paymentId
            ]
        ]);

        $contents = $response->getBody()->getContents();
        $decoded = json_decode($contents);

        $message = "Charge Created:";
        $this->output->writeln($message);

        $message = '<info>'. $contents .'</info>';
        $this->output->writeln($message);

        return $decoded;
    }

    private function listCharges()
    {
        $response = $this->request('GET', 'http://127.0.0.1:8000/api/charge');

        $message = "Listing Charges:";
        $this->output->writeln($message);

        $message = '<info>'. $response->getBody()->getContents() .'</info>';
        $this->output->writeln($message);
    }

    /**
     * @param $viewCharge
     */
    private function viewCharge($viewCharge)
    {
        $response = $this->request('GET', 'http://127.0.0.1:8000/api/charge/'. $viewCharge);

        $message = "View Charge:";
        $this->output->writeln($message);

        $message = '<info>'. $response->getBody()->getContents() .'</info>';
        $this->output->writeln($message);
    }
}
