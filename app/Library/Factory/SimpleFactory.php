<?php

namespace App\Library\Factory;

use App\Library\Factory\Contracts\Factory;
use Symfony\Component\Debug\Exception\ClassNotFoundException;

/**
 * Class SimpleFactory
 * @package App\Library\Factory
 */
class SimpleFactory implements Factory
{
    /**
     * @param $class
     * @return \Illuminate\Foundation\Application|mixed
     * @throws ClassNotFoundException
     */
    public function create($class)
    {
        if (is_null($class) || !class_exists($class)) {
            $message = 'Invalid Class: '. $class;
            throw new ClassNotFoundException($message, new \ErrorException());
        }

        return app($class);
    }
}
