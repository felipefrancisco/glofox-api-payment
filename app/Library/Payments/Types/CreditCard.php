<?php
/**
 * Created by PhpStorm.
 * User: felipefrancisco
 * Date: 04/08/17
 * Time: 23:18
 */

namespace App\Library\Payments\Types;

use App\Library\Payments\Types\Contracts\PaymentType as PaymentTypeContract;

/**
 * Class CreditCard
 * @package App\Library\Payments\Types
 */
class CreditCard implements PaymentTypeContract
{
    /**
     * @return string
     */
    public static function stringify()
    {
        return 'cc';
    }

    /**
     * @return mixed
     */
    public function validator()
    {
        return \App\Library\Payments\Validators\CreditCard::class;
    }

    /**
     * @return mixed
     */
    public function charger()
    {
        return \App\Library\Payments\Chargers\CreditCard::class;
    }

    /**
     * @return mixed
     */
    public function filter()
    {
        return \App\Library\Payments\Filters\CreditCard::class;
    }
}
