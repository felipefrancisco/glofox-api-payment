<?php
/**
 * Created by PhpStorm.
 * User: felipefrancisco
 * Date: 04/08/17
 * Time: 23:18
 */

namespace App\Library\Payments\Types;

use App\Library\Payments\Types\Contracts\PaymentType as PaymentTypeContract;

/**
 * Class DirectDebit
 * @package App\Library\Payments\Types
 */
class DirectDebit implements PaymentTypeContract
{
    /**
     * @return string
     */
    public static function stringify()
    {
        return 'dd';
    }

    /**
     * @return mixed
     */
    public function validator()
    {
        return \App\Library\Payments\Validators\DirectDebit::class;
    }

    /**
     * @return mixed
     */
    public function charger()
    {
        return \App\Library\Payments\Chargers\DirectDebit::class;
    }

    /**
     * @return mixed
     */
    public function filter()
    {
        return \App\Library\Payments\Filters\DirectDebit::class;
    }
}
