<?php

namespace App\Library\Payments\Types\Contracts;

interface PaymentType
{
    public function validator();

    public function charger();

    public static function stringify();
}
