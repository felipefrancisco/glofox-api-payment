<?php

namespace App\Library\Payments\Types;

use App\Library\Payments\Exceptions\InvalidPaymentTypeException;
use Illuminate\Support\Collection;

/**
 * Class PaymentTypeResolver
 * @package App\Library\Payments\Types
 */
class PaymentTypeResolver
{
    /**
     * Payment Types Collection
     * @var Collection
     */
    protected static $types;

    /**
     * PaymentTypeFactory instance
     * @var PaymentTypeFactory
     */
    protected $typeFactory;

    /**
     * PaymentTypeResolver constructor.
     * @param PaymentTypeFactory $typeFactory
     */
    public function __construct(PaymentTypeFactory $typeFactory)
    {
        $this->typeFactory = $typeFactory;

        if (!self::$types) {
            $types = [
                CreditCard::stringify()  => CreditCard::class,
                DirectDebit::stringify() => DirectDebit::class
            ];

            self::$types = new Collection($types);
        }
    }

    /**
     * @return Collection
     */
    protected function types()
    {
        return self::$types;
    }

    /**
     * @param $type
     * @return mixed
     */
    protected function find($type)
    {
        return $this->types()->get($type);
    }

    /**
     * @param $type
     * @return \Illuminate\Foundation\Application|mixed
     * @throws InvalidPaymentTypeException
     */
    public function resolve($type)
    {
        $type = $this->find($type);

        if (!$type) {
            throw new InvalidPaymentTypeException();
        }

        return $this->typeFactory->create($type);
    }
}
