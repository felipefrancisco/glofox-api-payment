<?php

namespace App\Library\Payments\Chargers\Contracts;

/**
 * Interface Charger
 * @package App\Library\Payments\Chargers\Contracts
 */
interface Charger
{
    /**
     * @param float $amount
     * @return mixed
     */
    public function charge(float $amount);
}
