<?php

namespace App\Library\Payments\Chargers;

use App\Library\Payments\Chargers\Contracts\Charger as ChargerContract;

/**
 * Class CreditCard
 * @package App\Library\Payments\Chargers
 */
class CreditCard implements ChargerContract
{
    /**
     * @param float $amount
     * @return float
     */
    public function charge(float $amount)
    {
        return $amount * 1.10;
    }
}
