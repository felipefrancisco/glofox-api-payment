<?php

namespace App\Library\Payments\Chargers;

use App\Library\Payments\Chargers\Contracts\Charger as ChargerContract;

/**
 * Class DirectDebit
 * @package App\Library\Payments\Chargers
 */
class DirectDebit implements ChargerContract
{
    /**
     * @param float $amount
     * @return float
     */
    public function charge(float $amount)
    {
        return $amount * 1.07;
    }
}
