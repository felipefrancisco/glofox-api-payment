<?php

namespace App\Library\Payments\Filters;

use App\Library\Payments\Filters\Contracts\PaymentFilter as PaymentFilterContract;

/**
 * Class CreditCard
 * @package App\Library\Payments\Filters
 */
class CreditCard implements PaymentFilterContract
{
    /**
     * @param array $data
     * @return array
     */
    public function filter(array $data) : array
    {
        $data = collect($data)
                    ->only(['id', 'name', 'type' , 'cc', 'ccv', 'expiry'])
                    ->all();

        return $data;
    }
}
