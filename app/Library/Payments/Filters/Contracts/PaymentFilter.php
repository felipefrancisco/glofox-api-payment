<?php

namespace App\Library\Payments\Filters\Contracts;

/**
 * Interface PaymentFilter
 * @package App\Library\Payments\Filters\Contracts
 */
interface PaymentFilter
{
    /**
     * @param array $data
     * @return array
     */
    public function filter(array $data) : array;
}
