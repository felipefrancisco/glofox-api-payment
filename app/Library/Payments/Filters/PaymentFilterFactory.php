<?php

namespace App\Library\Payments\Filters;

use App\Library\Factory\SimpleFactory;

/**
 * Class PaymentFilterFactory
 * @package App\Library\Payments\Filters
 */
class PaymentFilterFactory extends SimpleFactory
{
}
