<?php

namespace App\Library\Payments\Filters;

use App\Library\Payments\Filters\Contracts\PaymentFilter as PaymentFilterContract;

/**
 * Class DirectDebit
 * @package App\Library\Payments\Filters
 */
class DirectDebit implements PaymentFilterContract
{
    /**
     * @param array $data
     * @return array
     */
    public function filter(array $data) : array
    {
        $data = collect($data)
            ->only(['id', 'name', 'type' , 'iban'])
            ->all();

        return $data;
    }
}
