<?php

namespace App\Library\Payments\Exceptions;

/**
 * Class InvalidPaymentTypeException
 * @package App\Library\Payments\Exceptions
 */
class InvalidPaymentTypeException extends \ErrorException
{
}
