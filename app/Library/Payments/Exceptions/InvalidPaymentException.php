<?php

namespace App\Library\Payments\Exceptions;

/**
 * Class InvalidPaymentException
 * @package App\Library\Payments\Exceptions
 */
class InvalidPaymentException extends \ErrorException
{
    protected $message = "Invalid Payment.";
}
