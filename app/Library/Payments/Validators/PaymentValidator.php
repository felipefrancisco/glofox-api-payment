<?php

namespace App\Library\Payments\Validators;

use App\Library\Payments\Validators\Contracts\Validator as ValidatorContract;
use App\Library\Payments\Types\CreditCard;
use App\Library\Payments\Types\DirectDebit;
use Illuminate\Support\Collection;
use App\Library\Validators\Validator;

/**
 * Class PaymentValidator
 * @package App\Library\Payments\Validators
 */
class PaymentValidator extends Validator implements ValidatorContract
{
    /**
     * @return Collection
     */
    public function rules() : Collection
    {
        $creditCardType = CreditCard::stringify();
        $debitCardType  = DirectDebit::stringify();

        $typeRule = 'required|in:'. implode(',', [$creditCardType, $debitCardType]);

        $rules = new Collection([
            'name' => 'required|string',
            'type' => $typeRule
        ]);

        return $rules;
    }
}
