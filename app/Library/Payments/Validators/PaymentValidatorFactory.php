<?php

namespace App\Library\Payments\Validators;

use App\Library\Factory\SimpleFactory;

/**
 * Class PaymentValidatorFactory
 * @package App\Library\Payments\Validators
 */
class PaymentValidatorFactory extends SimpleFactory
{
}
