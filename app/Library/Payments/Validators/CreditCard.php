<?php
/**
 * Created by PhpStorm.
 * User: felipefrancisco
 * Date: 04/08/17
 * Time: 23:18
 */

namespace App\Library\Payments\Validators;

use App\Library\Payments\Validators\Contracts\Validator;
use Illuminate\Support\Collection;

/**
 * Class CreditCard
 * @package App\Library\Payments\Validators
 */
class CreditCard extends PaymentValidator implements Validator
{
    /**
     * @return Collection
     */
    public function rules() : Collection
    {
        $generalRules = parent::rules();

        $rules = [
            "cc" => "required|string",
            "expiry" => [
                'required',
                "string",
                'regex:/^(0[1-9]|1[0-2])\/[0-9]{2}/'
            ],
            "ccv" => [
                "required",
                "string",
                "regex:/^[0-9]{3,4}$/"
            ]
        ];

        return $generalRules->merge($rules);
    }
}
