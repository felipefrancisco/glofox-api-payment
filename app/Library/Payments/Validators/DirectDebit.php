<?php
/**
 * Created by PhpStorm.
 * User: felipefrancisco
 * Date: 04/08/17
 * Time: 23:18
 */

namespace App\Library\Payments\Validators;

use App\Library\Payments\Validators\Contracts\Validator;
use Illuminate\Support\Collection;

/**
 * Class DirectDebit
 * @package App\Library\Payments\Validators
 */
class DirectDebit extends PaymentValidator implements Validator
{
    /**
     * @return Collection
     */
    public function rules() : Collection
    {
        $generalRules = parent::rules();

        $rules = [
            "iban" => "required|string"
        ];

        return $generalRules->merge($rules);
    }
}
