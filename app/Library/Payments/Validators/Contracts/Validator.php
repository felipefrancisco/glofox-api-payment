<?php

namespace App\Library\Payments\Validators\Contracts;

use Illuminate\Support\Collection;

/**
 * Interface Validator
 * @package App\Library\Payments\Validators\Contracts
 */
interface Validator
{
    /**
     * @return Collection
     */
    public function rules() : Collection;
}
