<?php

namespace App\Library\Payments\Requests;

use App\Http\Requests\GlofoxRequest;
use App\Library\Payments\Exceptions\InvalidPaymentTypeException;
use App\Library\Payments\Validators\PaymentValidator;
use App\Library\Payments\Validators\PaymentValidatorFactory;
use App\Library\Payments\Validators\Contracts\Validator;
use App\Library\Payments\Types\PaymentTypeResolver;

/**
 * Class CreatePayment
 * @package App\Library\Payments\Requests
 */
class CreatePayment extends GlofoxRequest
{
    /**
     * @return bool
     */
    public function authorize() : bool
    {
        return true;
    }

    /**
     * @param PaymentTypeResolver $typeResolver
     * @param PaymentValidatorFactory $validatorFactory
     * @return array
     */
    public function rules(
        PaymentTypeResolver $typeResolver,
        PaymentValidatorFactory $validatorFactory
    ) : array {
        try {
            $type = $this->json->get('type');

            /** @var PaymentType $type */
            $type = $typeResolver->resolve($type);

            /** @var Validator $validator */
            $validator = $validatorFactory->create($type->validator());

            return $validator->rules()->toArray();
        } catch (InvalidPaymentTypeException $e) {
            $validator = $validatorFactory->create(PaymentValidator::class);
            return $validator->rules()->toArray();
        }
    }
}
