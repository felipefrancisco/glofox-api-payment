<?php

namespace App\Library\Charges\Validators\Contracts;

use Illuminate\Support\Collection;

/**
 * Interface Validator
 * @package App\Library\Charges\Validators\Contracts
 */
interface Validator
{
    /**
     * @return Collection
     */
    public function rules() : Collection;
}
