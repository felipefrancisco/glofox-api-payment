<?php

namespace App\Library\Charges\Validators;

use Illuminate\Support\Collection;
use App\Library\Charges\Validators\Contracts\Validator as ValidatorContract;
use App\Library\Validators\Validator;

/**
 * Class ChargeValidator
 * @package App\Library\Charges\Validators
 */
class ChargeValidator extends Validator implements ValidatorContract
{
    /**
     * @return Collection
     */
    public function rules() : Collection
    {
        $rules = Collection::make([
            'amount' => 'required|numeric|min:0',
            'payment_id' => 'required|string'
        ]);

        return $rules;
    }
}
