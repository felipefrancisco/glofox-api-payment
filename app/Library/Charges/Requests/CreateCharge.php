<?php

namespace App\Library\Charges\Requests;

use App\Http\Requests\GlofoxRequest;
use App\Library\Charges\Validators\ChargeValidator;
use App\Library\Charges\Validators\ChargeValidatorFactory;
use App\Library\Charges\Validators\Contracts\Validator;

/**
 * Class CreateCharge
 * @package App\Library\Charges\Requests
 */
class CreateCharge extends GlofoxRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @param ChargeValidatorFactory $validatorFactory
     * @return array
     */
    public function rules(ChargeValidatorFactory $validatorFactory)
    {
        /** @var Validator $validator */
        $validator = $validatorFactory->create(ChargeValidator::class);

        return $validator->rules()->toArray();
    }
}
