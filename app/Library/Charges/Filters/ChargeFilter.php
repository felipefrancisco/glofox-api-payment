<?php

namespace App\Library\Charges\Filters;

use App\Library\Charges\Filters\Contracts\ChargeFilter as ChargeFilterContract;

/**
 * Class ChargeFilter
 * @package App\Library\Charges\Filters
 */
class ChargeFilter implements ChargeFilterContract
{
    /**
     * @param array $data
     * @return array
     */
    public function filter(array $data) : array
    {
        $data = collect($data)
            ->only(['id', 'amount' , 'payment_id'])
            ->all();

        return $data;
    }
}
