<?php

namespace App\Library\Charges\Filters\Contracts;

/**
 * Interface ChargeFilter
 * @package App\Library\Charges\Filters\Contracts
 */
interface ChargeFilter
{
    /**
     * @param array $data
     * @return array
     */
    public function filter(array $data) : array;
}
