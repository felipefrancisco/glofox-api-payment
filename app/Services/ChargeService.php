<?php

namespace App\Services;

use App\Library\Charges\Validators\ChargeValidator;
use App\Library\Charges\Filters\ChargeFilter;
use App\Library\Charges\Filters\ChargeFilterFactory;
use App\Library\Charges\Repositories\ChargeRepository;
use App\Library\Payments\Chargers\ChargerFactory;
use App\Library\Payments\Chargers\Contracts\Charger;
use App\Library\Payments\Exceptions\InvalidPaymentException;
use App\Library\Payments\Repositories\PaymentRepository;
use App\Library\Payments\Types\PaymentTypeResolver;
use App\Models\Charge;
use App\Models\Payment;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Class ChargeService
 * @package App\Services
 */
class ChargeService extends Service
{
    /**
     * ChargeRepository instance.
     * @var ChargeRepository
     */
    protected $chargeRepository;

    /**
     * ChargeFilterFactory instance.
     * @var ChargeFilterFactory
     */
    protected $filterFactory;

    /**
     * ChargerFactory instance.
     * @var ChargerFactory
     */
    protected $chargerFactory;

    /**
     * PaymentTypeResolver instance.
     * @var PaymentTypeResolver
     */
    protected $paymentTypeResolver;

    /**
     * ChargeValidator instance.
     * @var ChargeValidator
     */
    protected $chargeValidator;

    /**
     * PaymentRepository instance.
     * @var PaymentRepository
     */
    protected $paymentRepository;

    /**
     * ChargeService constructor.
     * @param ChargeRepository $chargeRepository
     * @param ChargeFilterFactory $filterFactory
     * @param ChargerFactory $chargerFactory
     * @param PaymentTypeResolver $paymentTypeResolver
     * @param ChargeValidator $chargeValidator
     * @param PaymentRepository $paymentRepository
     */
    public function __construct(
        ChargeRepository $chargeRepository,
        ChargeFilterFactory $filterFactory,
        ChargerFactory $chargerFactory,
        PaymentTypeResolver $paymentTypeResolver,
        ChargeValidator $chargeValidator,
        PaymentRepository $paymentRepository
    ) {
        $this->chargeRepository = $chargeRepository;
        $this->filterFactory = $filterFactory;
        $this->chargerFactory = $chargerFactory;
        $this->paymentTypeResolver = $paymentTypeResolver;
        $this->chargeValidator = $chargeValidator;
        $this->paymentRepository = $paymentRepository;
    }

    /**
     * @param array $data
     * @return Charge
     * @throws InvalidPaymentException
     */
    public function create(array $data) : Charge
    {
        /** @var ChargeFilter $inputFilter */
        $inputFilter = $this->filterFactory->create(ChargeFilter::class);

        /** @var array $data */
        $data = $inputFilter->filter($data);

        $this->chargeValidator->prepare($data)
                              ->validate();

        try {

            /** @var Payment $payment */
            $payment = $this->paymentRepository->find($data['payment_id']);
        } catch (ModelNotFoundException $e) {
            throw new InvalidPaymentException();
        }

        $type = $this->paymentTypeResolver->resolve($payment->type);

        /** @var Charger $charger */
        $charger = $this->chargerFactory->create($type->charger());

        $data['amount'] = $charger->charge($data['amount']);

        /** @var Charge $charge */
        $charge = $this->chargeRepository->create($data);

        // We could fire an event here:
        // event(new ChargeWasCreated($charge));

        return $charge;
    }
}
