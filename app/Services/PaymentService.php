<?php

namespace App\Services;

use App\Library\Payments\Filters\Contracts\PaymentFilter;
use App\Library\Payments\Filters\PaymentFilterFactory;
use App\Library\Payments\Repositories\PaymentRepository;
use App\Library\Payments\Types\PaymentTypeResolver;
use App\Library\Payments\Validators\PaymentValidatorFactory;
use App\Library\Validators\Validator;
use App\Models\Payment;

/**
 * Class PaymentService
 * @package App\Services
 */
class PaymentService extends Service
{
    /**
     * PaymentRepository instance.
     * @var $paymentRepository
     */
    protected $paymentRepository;
    /**
     * PaymentTypeResolver instance.
     * @var $typeResolver
     */
    protected $typeResolver;

    /**
     * PaymentFilterFactory instance.
     * @var $filterFactory
     */
    protected $filterFactory;

    /**
     * PaymentValidatorFactory instance.
     * @var $validatorFactory
     */
    protected $validatorFactory;

    /**
     * PaymentService constructor.
     * @param PaymentRepository $paymentRepository
     * @param PaymentTypeResolver $typeResolver
     * @param PaymentFilterFactory $filterFactory
     * @param PaymentValidatorFactory $validatorFactory
     */
    public function __construct(
        PaymentRepository $paymentRepository,
        PaymentTypeResolver $typeResolver,
        PaymentFilterFactory $filterFactory,
        PaymentValidatorFactory $validatorFactory
    ) {
        $this->paymentRepository = $paymentRepository;
        $this->typeResolver = $typeResolver;
        $this->filterFactory = $filterFactory;
        $this->validatorFactory = $validatorFactory;
    }

    /**
     * @param array $data
     * @return Payment
     */
    public function create(array $data) : Payment
    {
        if (!isset($data['type'])) {
            throw new \RuntimeException();
        }

        /** @var PaymentType $type */
        $type = $this->typeResolver->resolve($data['type']);

        /** @var PaymentFilter $inputFilter */
        $inputFilter = $this->filterFactory->create($type->filter());

        /** @var array $data */
        $data = $inputFilter->filter($data);

        /** @var Validator $validator */
        $validator = $this->validatorFactory->create($type->validator());
        $validator->prepare($data)
                    ->validate();

        /** @var Payment $payment */
        $payment = $this->paymentRepository->create($data);

        // We could fire an event here:
        // event(new PaymentWasCreated($payment));

        return $payment;
    }
}
