<?php

namespace App\Providers;

use App\Library\Charges\Validators\ChargeValidatorFactory;
use App\Library\Payments\Types\PaymentTypeFactory;
use App\Library\Payments\Types\PaymentTypeResolver;
use App\Library\Payments\Validators\PaymentValidatorFactory;
use Illuminate\Support\ServiceProvider;

/**
 * Class AppServiceProvider
 * @package App\Providers
 */
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(PaymentTypeFactory::class, function () {
            return new PaymentTypeFactory();
        });

        $this->app->singleton(ChargeValidatorFactory::class, function () {
            return new ChargeValidatorFactory();
        });

        $this->app->singleton(PaymentValidatorFactory::class, function () {
            return new PaymentValidatorFactory();
        });

        $this->app->bind(PaymentTypeResolver::class, function ($app) {

            /** @noinspection PhpUndefinedMethodInspection */
            return new PaymentTypeResolver($app->make(PaymentTypeFactory::class));
        });
    }
}
