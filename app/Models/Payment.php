<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

/**
 * Class Payment
 * @package App\Models
 *
 * @property string $id
 * @property string $name
 * @property string $type
 * @property string $iban
 * @property date   $expiry
 * @property string $cc
 * @property string $ccv
 */
class Payment extends Eloquent
{
    protected $fillable = [
        'name',
        'type',
        'iban',
        'expiry',
        'cc',
        'ccv'
    ];
}
