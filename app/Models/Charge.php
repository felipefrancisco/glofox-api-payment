<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

/**
 * Class Charge
 * @package App\Models
 *
 * @property string $id
 * @property string $payment_id
 * @property float  $amount
 */
class Charge extends Eloquent
{
    protected $fillable = [
        'amount',
        'payment_id'
    ];
}
