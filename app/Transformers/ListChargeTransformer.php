<?php

namespace App\Transformers;

use League\Fractal;
use App\Models\Charge;

/**
 * Class ListChargeTransformer
 * @package App\Transformers
 */
class ListChargeTransformer extends Fractal\TransformerAbstract
{
    /**
     * @param Charge $charge
     * @return array
     */
    public function transform(Charge $charge)
    {
        $collection = collect($charge->toArray());

        $response = $collection->only(['payment_id', 'amount'])
                               ->all();

        return $response;
    }
}
