<?php

namespace App\Transformers;

use League\Fractal;
use App\Models\Charge;

/**
 * Class ChargeTransformer
 * @package App\Transformers
 */
class ChargeTransformer extends Fractal\TransformerAbstract
{
    /**
     * @param Charge $charge
     * @return array
     */
    public function transform(Charge $charge)
    {
        $collection = collect($charge->toArray());

        $response = $collection->only(['_id', 'payment_id', 'amount'])
                               ->all();

        return $response;
    }
}
