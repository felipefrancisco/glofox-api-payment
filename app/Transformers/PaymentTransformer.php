<?php

namespace App\Transformers;

use League\Fractal;
use App\Models\Payment;

/**
 * Class PaymentTransformer
 * @package App\Transformers
 */
class PaymentTransformer extends Fractal\TransformerAbstract
{
    /**
     * @param Payment $payment
     * @return array
     */
    public function transform(Payment $payment)
    {
        $collection = collect($payment->toArray());

        $response = $collection->only(['_id', 'name', 'type', 'iban', 'cc', 'ccv', 'expiry'])
                               ->all();

        return $response;
    }
}
